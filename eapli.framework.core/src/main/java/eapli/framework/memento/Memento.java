package eapli.framework.memento;

/**
 * an opaque object representing the internal state of an object. GoF design pattern.
 * 
 * @author Paulo Gandra Sousa
 *
 */
public interface Memento {

}
