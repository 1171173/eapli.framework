/**
 *
 */
package eapli.framework.domain.model.math;

import java.util.Arrays;
import java.util.function.BinaryOperator;
import java.util.function.DoubleSupplier;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.EqualsBuilder;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.util.Preconditions;
import eapli.framework.util.function.ArrayOfDoubleCollector;

/**
 * An immutable vector.
 *
 * Vector indexes are 1-based, so a Vector with length 4 will have indexes 1, 2,
 * 3 and 4
 *
 * @author Paulo Gandra Sousa
 *
 */
public class Vector implements ValueObject {

    private static final String VECTOR_TYPE_MUST_BE_THE_SAME = "vector type must be the same";
    private static final long serialVersionUID = -3010865637192089056L;

    public enum VectorType {
        ROW, COLUMN
    }

    private final int dimensions;
    private final double[] data;
    private final VectorType type;

    public Vector(final double[] src, final VectorType type) {
        dimensions = src.length;
        this.type = type;
        data = Arrays.copyOf(src, dimensions);
    }

    public Vector(final Vector other) {
        dimensions = other.dimensions;
        type = other.type;
        data = Arrays.copyOf(other.data, other.dimensions);
    }

    public Vector(final VectorType type, final int maxSize, final Stream<Double> src) {
        dimensions = maxSize;
        this.type = type;
        data = new double[dimensions];
        src.limit(maxSize).collect(ArrayOfDoubleCollector.collector(data));
    }

    public Vector(final VectorType type, final int maxSize, final DoubleStream src) {
        dimensions = maxSize;
        this.type = type;
        data = src.limit(maxSize).toArray();
    }

    private Vector(final VectorType type, final int maxSize, final DoubleSupplier src) {
        dimensions = maxSize;
        this.type = type;
        data = new double[dimensions];
        for (int i = 0; i < maxSize; i++) {
            data[i] = src.getAsDouble();
        }
    }

    private Vector(final Vector a, final Vector b, final BinaryOperator<Double> op) {
        Preconditions.areEqual(a.dimensions, b.dimensions);
        Preconditions.areEqual(a.type, b.type, VECTOR_TYPE_MUST_BE_THE_SAME);

        dimensions = a.dimensions;
        type = a.type;
        data = new double[dimensions];
        for (int i = 0; i < dimensions; i++) {
            data[i] = op.apply(a.data[i], b.data[i]);
        }
    }

    /**
     * creates a vector with all elements with the value 0.0
     */
    public static Vector zero(final int n, final VectorType type) {
        return zero(n, type, 0.0);
    }

    /**
     * creates a "zero" vector with the same given value
     */
    public static Vector zero(final int n, final VectorType type, final double zero) {
        return new Vector(type, n, () -> zero);
    }

    /**
     * adds support for Java 8 Streams
     *
     * @return the elements of the Vector as a stream
     */
    public DoubleStream stream() {
        return Arrays.stream(data);
    }

    /**
     * indexes are 1-based
     *
     * @param i
     * @return
     */
    public double elementAt(final int i) {
        return data[i - 1];
    }

    public Vector normalized() {
        final Double denominator = Math.sqrt(sum());

        return new Vector(type, dimensions, Arrays.stream(data).map(e -> e / denominator));
    }

    private double sum() {
        double sum = 0;
        for (final double x : data) {
            sum += x;
        }
        return sum;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Vector)) {
            return false;
        }
        final Vector other = (Vector) obj;
        return new EqualsBuilder().append(dimensions, other.dimensions).append(type, other.type)
                .append(data, other.data).isEquals();
    }

    @Override
    public int hashCode() {
        final HashCoder coder = new HashCoder().of(type).of(data);
        return coder.code();
    }

    public Vector add(final Vector b) {
        return new Vector(this, b, (x, y) -> x + y);
    }

    public Vector subtract(final Vector b) {
        return new Vector(this, b, (x, y) -> x - y);
    }

    public Vector crossProduct(final Vector b) {
        return new Vector(this, b, (x, y) -> x * y);
    }

    /**
     * calculates the dot product of two vectors
     *
     * @param other
     * @return the dot product of two vectors
     */
    public double dotProduct(final Vector other) {
        Preconditions.areEqual(dimensions, other.dimensions);
        Preconditions.areEqual(type, other.type, VECTOR_TYPE_MUST_BE_THE_SAME);

        double accum = 0;
        for (int i = 0; i < dimensions; i++) {
            accum += (data[i] * other.data[i]);
        }
        return accum;
    }

    /**
     * return a new vector obtained by multiplying a vector by a scalar
     *
     * @param k
     * @return
     */
    public Vector scale(final double k) {
        return new Vector(type, dimensions, Arrays.stream(data).map(e -> k * e));
    }

    /**
     * returns the magnitude (length) of the vector
     *
     * @return
     */
    public double magnitude() {
        double accum = 0;
        for (final double x : data) {
            accum += (x * x);
        }
        return Math.sqrt(accum);
    }

    public boolean isUnit() {
        return magnitude() == 1.0;
    }

    public int dimensions() {
        return dimensions;
    }
}
