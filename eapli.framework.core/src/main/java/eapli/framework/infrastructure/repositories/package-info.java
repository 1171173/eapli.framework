/**
 * @author pgsou_000
 *
 */

/*
@startuml
skinparam classAttributeIconSize 0

interface Repository<T, K extends Serializable> {
    void delete(T entity)
    void delete(K entityId)
    T save(T entity)
    Iterable<T> findAll();
    Optional<T> findOne(K id);
    long count();
}

class JpaAutoTxRepository<T, K extends Serializable> {

    # final JpaBaseRepository<T, K> repo
    - final TransactionalContext autoTx

    + TransactionalContext buildTransactionalContext(String persistenceUnitName)
    + TransactionalContext context()
    + boolean isInTransaction()
    + void delete(T entity)
    + T save(T entity)
    + Iterable<T> findAll()
    + Optional<T> findOne(K id)
    + long count()
    + Iterator<T> iterator()
    # List<T> match(String where)
    # Optional<T> matchOne(String where)
}

interface IterableRepository<T, K extends Serializable> {
    Iterator<T> iterator(int pagesize);
    T first();
    Iterable<T> first(int n);
}

class JpaBaseRepository<T, K extends Serializable> {
    # Class<T> entityClass
    - EntityManagerFactory emFactory
    - EntityManager entityManager

    # EntityManagerFactory entityManagerFactory()
    # EntityManager entityManager()
    + T create(T entity)
    + Optional<T> read(K id)
    + Optional<T> findOne(K id)
    + T update(T entity)
    + void delete(T entity)
    + void deletebyPK(K entityId)
    + long count()
    + boolean contains(K key)
    + boolean add(T entity)
    + T save(T entity)
    # TypedQuery<T> queryAll()
    # TypedQuery<T> queryAll(String where, Map<String, Object> params)
    + List<T> first(int n)
}

class JpaWithTransactionalContextRepository<T, K extends Serializable> {
    # EntityManagerFactory entityManagerFactory()
    # EntityManager entityManager()
    # TransactionalContext context()
}

class JpaTransactionalRepository<T, K extends Serializable> {
    + void delete(final T entity)
    + void delete(final K entityId)
    + T create(final T entity)
    + T save(T entity)
}

interface TransactionalContext {
   void beginTransaction()
   void commit()
   void rollback()
   void close()
}

class JpaTransactionalContext {
   - final String persistenceUnitName
   - static volatile EntityManagerFactory singletonEMF
     EntityManager entityManager()
   + void beginTransaction()
   + void commit()
   + void rollback()
   + void close()
}

Repository  <|..  JpaAutoTxRepository
Repository  <|..  IterableRepository
IterableRepository  <|..  JpaBaseRepository
JpaBaseRepository  <|-- JpaWithTransactionalContextRepository
JpaWithTransactionalContextRepository  <|-- JpaTransactionalRepository

TransactionalContext <-- "autoTx" JpaAutoTxRepository
TransactionalContext <.. JpaWithTransactionalContextRepository
JpaBaseRepository <--  "repo" JpaAutoTxRepository

class JpaTransactionalContext implements TransactionalContext

@enduml

 */
package eapli.framework.infrastructure.repositories;