/**
 *
 */
package eapli.framework.domain.model.math.numeral;

/**
 * @author sou03408
 *
 */
public class BinaryNumeralTest extends NumeralTestBase {

    @Override
    protected int getBase() {
        return 2;
    }
}
